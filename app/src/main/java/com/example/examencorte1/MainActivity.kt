package com.example.examencorte1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
class MainActivity : AppCompatActivity() {
    // Declaración de variables

    // EditText
    private lateinit var txtNumeroCuenta: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtNombreBanco: EditText
    private lateinit var txtSaldo: EditText

    // Botones
    private lateinit var btnEnviar: Button
    private lateinit var btnSalir: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        btnEnviar.setOnClickListener {
            enviar()
        }
        btnSalir.setOnClickListener {
            salir()
        }
    }

    private fun iniciarComponentes() {
        // Cajas de Texto
        txtNumeroCuenta = findViewById(R.id.txtNumeroCuenta)
        txtNombre = findViewById(R.id.txtNombre)
        txtNombreBanco = findViewById(R.id.txtNombreBanco)
        txtSaldo = findViewById(R.id.txtSaldo)

        // Botones
        btnEnviar = findViewById(R.id.btnEnviar)
        btnSalir = findViewById(R.id.btnSalir)
    }

    private fun enviar() {
        val numCuenta: String
        val nombre: String
        val banco: String
        val saldo: String

        banco = applicationContext.resources.getString(R.string.strBanco)

        // Hacer el paquete para enviar información
        val bundle = Bundle()
        bundle.putString("nombre", txtNombre.text.toString())
        bundle.putString("banco", txtNombreBanco.text.toString())
        bundle.putString("saldo", txtSaldo.text.toString())

        // Crear el intent para llamar a otra actividad
        val intent = Intent(this@MainActivity, CuentaBancoActivity::class.java)
        intent.putExtras(bundle)

        // Iniciar la actividad esperando o no respuesta
        startActivity(intent)
    }

    private fun salir() {
        finish()
    }
}
