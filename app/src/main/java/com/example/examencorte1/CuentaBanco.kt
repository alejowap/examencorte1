package com.example.examencorte1

class CuentaBanco(private var numCuenta: Int, private var nombre: String, private var banco: String, private var saldo: Float) {

    fun depositar(dep: Float): Boolean {
        saldo += dep
        return true
    }

    fun retirar(ret: Float): Boolean {
        if (ret <= saldo) {
            saldo -= ret
            return true
        } else {
            return false
        }
    }

    fun getNumCuenta(): Int {
        return numCuenta
    }

    fun setNumCuenta(numCuenta: Int) {
        this.numCuenta = numCuenta
    }

    fun getNombre(): String {
        return nombre
    }

    fun setNombre(nombre: String) {
        this.nombre = nombre
    }

    fun getBanco(): String {
        return banco
    }

    fun setBanco(banco: String) {
        this.banco = banco
    }

    fun getSaldo(): Float {
        return saldo
    }

    fun setSaldo(saldo: Float) {
        this.saldo = saldo
    }
}
